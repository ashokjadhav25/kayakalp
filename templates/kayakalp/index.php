<?php
defined('_JEXEC') or die;

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>

<meta name="description" content="Free Web tutorials">
<meta name="keywords" content="HTML,CSS,XML,JavaScript">
<meta name="author" content="Hege Refsnes">
<title><?php echo JText::_('TPL_KAYAKALP_TITLE'); ?></title>
	<link href='<?php echo (JURI::base() . 'templates/system/css/system.cs'); ?>' rel='stylesheet' type='text/css' />
	<link href='<?php echo (JURI::base() . 'templates/' . $this->template . '/css/default.css'); ?>' rel='stylesheet' type='text/css' />
	<link href='<?php echo (JURI::base() . 'templates/' . $this->template . '/css/stylesheet.css'); ?>' rel='stylesheet' type='text/css' />
	<link href='<?php echo (JURI::base() . 'templates/' . $this->template . '/css/jquery.bxslider.css'); ?>' rel='stylesheet' type='text/css' />
	
	<script type="text/javascript"  src='<?php echo (JURI::base() . 'templates/' . $this->template . '/js/modernizr-2.8.3-respond-1.4.2.min.js');?>'></script>
	<script type="text/javascript"  src='<?php echo (JURI::base() . 'templates/' . $this->template . '/js/jquery-1.9.1.min.js');?>'></script>
	<script type="text/javascript"  src='<?php echo (JURI::base() . 'templates/' . $this->template . '/js/jquery.bxslider.min.js');?>'></script>
	<script type="text/javascript"  src='<?php echo (JURI::base() . 'templates/' . $this->template . '/js/custom.js');?>'></script>
	<script type="text/javascript" src='<?php echo (JURI::base() . 'templates/' . $this->template . '/js/jquery.min.js');?>'></script>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

<!-- <script type="text/javascript">
    jQuery(document).ready(function($){
      $('.lightbox').lightbox();
    });
</script>
 -->


</head>

<body>
<!--wrapper-starts-->
<div id="wrapper">

<!--Top Menu-starts-->
	<div class="top-menu clearfix">
    	<div class="container">
		  <div class="top-mneu-inner"> 
			   <ul class="top-nav">
				  <li class="select-text"><?php echo JText::_('TPL_KAYAKALP_SELECT_LANGUAGE'); ?></li>
				    <jdoc:include type="modules" name="lang" style="none" />
			   </ul>
          </div>
		</div>
    </div>
<!--Top Menu-ends-->

<!--header-starts-->
	<header class="clearfix">
    	<div class="container">
		   <div class="logo"><a href="#"><img src="<?php echo (JURI::base() . 'templates/' . $this->template . '/images/logo.jpg'); ?>" alt=""></a></div>
           <div class="right-symbol"><a href="#"><img src="<?php echo (JURI::base() . 'templates/' . $this->template . '/images/right-symbol.jpg'); ?>" alt=""></a></div>
		</div>
    </header>
<!--header-ends-->

<!--nav-starts-->
	<nav class="clearfix">
    	<div class="container">
		
		   <div class="menu"> 
				<jdoc:include type="modules" name="menu" style="well" />
		   </div> 
		</div>
    </nav>
<!--nav-ends-->

<!--Banner-starts-->
		<?php
		 if($_REQUEST['Itemid']== 117)
		{
            ?>    

	<div class="banner clearfix">
    	<div class="container">
          <iframe width="100%" height="260" frameborder="0" allowfullscreen="" style="border:0 none; float:left;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3770.288226161592!2d72.88471144999998!3d19.0950075!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7c8651ac8dc15%3A0xaebff96b88bb256e!2sLokhandwala+Park%2C+Mayfair+Industrial+Area%2C+Safed+Pul%2C+Sakinaka%2C+Mumbai%2C+Maharashtra+400072!5e0!3m2!1sen!2sin!4v1440489975149"></iframe>
		</div>
    </div>
	<?php
		}else
		{
      ?> 

	  <div class="banner clearfix">
    	<div class="container">
          <img src="<?php echo (JURI::base() . 'templates/' . $this->template . '/images/banner_new.jpg'); ?>" alt=""/>
		</div>
    </div>
	<?php
		}
      ?> 
	  
<!--banner-ends-->

<!--section-starts-->
	<section class="clearfix">
    	<div class="container">
           <div class="Border content">

			 <div class="left-content">

			   <h2><?php echo JText::_('TPL_KAYAKALP_SEX_DISORDERS'); ?></h2>
               
			   <h3><?php echo JText::_('TPL_KAYAKALP_MALE_DISORDER'); ?></h3>
			   <div class="menu2"> 
					<jdoc:include type="modules" name="menu_male" style="well" />
				</div>
    			<h3><?php echo JText::_('TPL_KAYAKALP_FEMALE_DISORDER'); ?></h3>
			    <div class="menu2"> 
					<jdoc:include type="modules" name="menu_female" style="well" />
				</div>
			 </div>
             
			 <div class="right-content">
				 <jdoc:include type="component" />
				 <?php
					if($_REQUEST['Itemid']== 110 || $_REQUEST['Itemid']== 117)
					{
					?>   
					<div class="Box-main3">
				  
				   <div class="Box3">
						 <jdoc:include type="modules" name="address1" style="well" />
				   </div>

				   <div class="Box3 last-child">
						 <jdoc:include type="modules" name="address2" style="well" />
				   </div>
				<?php 
					}
				?>

			 <?php
			 if($_REQUEST['Itemid']== 101)
			{
            ?>    
				<div class="Box-main2">
				  
				   <div class="Box2 Top-box-bg">
						 <h3><?php echo JText::_('TPL_KAYAKALP_SERVICES'); ?></h3>
						 <jdoc:include type="modules" name="services" style="well" />
						 
				   </div>

				   <div class="Box2 Top-box-bg">
						 <h3><?php echo JText::_('TPL_KAYAKALP_AWARDS'); ?></h3>
						 <jdoc:include type="modules" name="awards" style="well" />
				   </div>

				   <div class="Box2 Top-box-bg last-child">
						<h3><?php echo JText::_('TPL_KAYAKALP_AYURVEDA'); ?></h3>
						<jdoc:include type="modules" name="ayurveda" style="well" />
				   </div>

				</div>

				<div class="Box-main2">
				  
				   <div class="Box2">
						 <h2 class="whatnews-icon"><?php echo JText::_('TPL_KAYAKALP_WHATS_NEW'); ?></h2>
						 <marquee height="107px" onmouseout="this.setAttribute('scrollamount', 2, 2);" onmouseover="this.setAttribute('scrollamount',  0, 0);" id="myMarquee" scrollamount="2" direction="up">
						 <jdoc:include type="modules" name="news_events" style="well" /></marquee>
				   </div>

				   <div class="Box2">
						 <h2 class="testimonial-icon"><?php echo JText::_('TPL_KAYAKALP_TESTIMONIAL'); ?></h2>
						 <jdoc:include type="modules" name="testimonials" style="well" />
				   </div>

				   <div class="Box2 last-child">
						<h2 class="askexpert-icon"><?php echo JText::_('TPL_KAYAKALP_ASK_EXPERT'); ?></h2>
						<jdoc:include type="modules" name="ask_expert" style="well" />
				   </div>

				</div>
				<?php 
					}
				?>

			 </div>

           </div>
        </div>
    </section>
<!--section-ends-->

<!--footer-starts-->
	<footer class="">
    	<div class="container">
		    <div class="Footer-inner">
				<div class="Footer-menu"> 
					<jdoc:include type="modules" name="menu_footer" style="well" />
			    </div> 
				<p class="copyrights-text"><?php echo JText::_('TPL_KAYAKALP_COPYRIGHT'); ?></p>
			 </div>
		</div>
    </footer>
<!--footer-ends-->
</div>
<!--wrapper-starts-->

<link href='<?php echo (JURI::base() . 'templates/' . $this->template . '/SpryAssets/SpryAccordion.css'); ?>' rel='stylesheet' type='text/css' />
<script type="text/javascript"  src='<?php echo (JURI::base() . 'templates/' . $this->template . '/SpryAssets/SpryAccordion.js');?>'></script>
<script type="text/javascript">
	var Accordion1 = new Spry.Widget.Accordion("Accordion1");
</script>
</body>
</html>


