<?php 
/**
 * @package Huge IT Gallery
 * @author Huge-IT
 * @copyright (C) 2014 Huge IT. All rights reserved.
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 * @website		http://www.huge-it.com/
 **/
?>
<?php defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.formvalidation');
JHtml::_('bootstrap.tooltip');
JHtml::_('formbehavior.chosen', 'select');
?>
<script type="text/javascript">
    Joomla.submitbutton = function (task)
    {
        if (task == 'general.apply1') {
            alert("General Settings are disabled in free version. If you need those functionalityes, you need to buy the commercial version.");
        }
        else if (task == 'general.save')
        {
            Joomla.submitform('apply1', document.getElementById('application-form'));
            alert("General Settings are disabled in free version. If you need those functionalityes, you need to buy the commercial version.");
        } else if (task == 'general.cancel') {
            Joomla.submitform('apply1', document.getElementById('application-form'));
        }
    }
</script>
<div class="slider-options-head" style="margin: 0px 0 0 -1px;">
    <div style="float: left;">
        <div><a href="http://huge-it.com/joomla-extensions-gallery-user-manual/" target="_blank">User Manual</a></div>
        <div>This section allows you to configure the Gallery options. <a href="http://huge-it.com/joomla-gallery" target="_blank">More...</a></div>
        <div>These options are made for pro users, and are displayed only for demonstration. Unfortunatelly free users are unable to use them.</div>
    </div>
    <div style="float: right;">
        <a style="position: relative;right: 50px;" class="header-logo-text" href="http://huge-it.com/joomla-gallery/" target="_blank">
            <div><img width="177px" src="<?php echo JUri::root() ?>media/com_gallery/images/huge-it1.png" /></div>
            <div style="margin: 2px 0px 5px 21px;">Get the full version</div>
        </a>
    </div>
</div>
<div style="clear: both"></div>
<div id="j-sidebar-container" class="j-sidebar-container j-sidebar-visible">
    <div id="j-toggle-sidebar-wrapper">
	<div id="j-toggle-sidebar-header" class="j-toggle-sidebar-header" style="padding: 10px 0 0px 20px;"> 
	<div id="sidebar" class="sidebar">
            <div class="sidebar-nav">
                <ul id="submenu" class="nav nav-list">
                    <li>
                            <a href="index.php?option=com_gallery">Huge - IT Gallery</a>
                    </li>
                    <li class="active">
                            <a href="index.php?option=com_gallery&amp;view=general">General Options</a>
                    </li>
                     <li>
                            <a href="index.php?option=com_gallery&amp;view=lightbox">Lightbox Options</a>
                    </li>
                    <li>
                            <a href="index.php?option=com_gallery&amp;view=featured">Featured Products</a>
                    </li>
                </ul>
            <div>
	</div>
	<div id="j-toggle-sidebar"></div>
            </div>
		</div>
        </div>
    </div>
</div>
<div id="j-main-container" class="span10 j-toggle-main">
<form action="<?php echo JRoute::_('index.php?option=com_gallery&views=galleries'); ?>" id="application-form" method="post" name="adminForm" class="form-validate">
    <div class="row-fluid">
        <div class="span10" style="width: 100%">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#page-site" data-toggle="tab"><?php echo JText::_('COM_CONFIG_BLOCKTOGGLE'); ?></a></li>
                <li><a href="#page-system" data-toggle="tab"><?php echo JText::_('COM_CONFIG_CONTENTSLIDER'); ?></a></li>
                <li><a href="#page-server" data-toggle="tab"><?php echo JText::_('COM_CONFIG_LIGHTBOX_GALLERY'); ?></a></li>
                <li><a href="#page-perm" data-toggle="tab"><?php echo JText::_('COM_CONFIG_SLIDER'); ?></a></li>
                <li><a href="#page-filters" data-toggle="tab"><?php echo JText::_('COM_CONFIG_TEXT_THUMBNAILS'); ?></a></li>
                <li><a href="#page-filters_" data-toggle="tab"><?php echo JText::_('COM_CONFIG_TEXT_JUSTIFIED'); ?></a></li>
            </ul>
            
            <div id="config-document" class="tab-content" class="dconfig">
                <div id="page-site" class="tab-pane active">
                    <div class="row-fluid">
                        <?php echo $this->loadTemplate('blocks'); ?>
                    </div>
                </div>
                <div id="page-system" class="tab-pane">
                    <div class="row-fluid">
                        <?php echo $this->loadTemplate('content'); ?>    
                    </div>
                </div>
                <div id="page-server" class="tab-pane">
                    <div class="row-fluid">
                        <?php echo $this->loadTemplate('lightbox'); ?>    
                    </div>
                </div>
                <div id="page-perm" class="tab-pane">
                    <div class="row-fluid">
                        <?php echo $this->loadTemplate('slider'); ?>      
                    </div>
                </div>
                <div id="page-filters" class="tab-pane">
                    <div class="row-fluid">
                        <?php echo $this->loadTemplate('thumbnails'); ?>     
                    </div>
                </div>
                <div id="page-filters_" class="tab-pane">
                    <div class="row-fluid">
                        <?php echo $this->loadTemplate('justified'); ?>  
                    </div>
                </div>
                </div>
        </div>
    </div>
    <div>
        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>
</div>