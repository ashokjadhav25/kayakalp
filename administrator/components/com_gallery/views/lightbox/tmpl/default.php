<?php 
/**
 * @package Huge IT Gallery
 * @author Huge-IT
 * @copyright (C) 2014 Huge IT. All rights reserved.
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 * @website		http://www.huge-it.com/
 **/
?>
<?php defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.tooltip');

JHtml::_('behavior.formvalidation');
?>

<script type="text/javascript">
    Joomla.submitbutton = function (task)
    {
       
        if (task == 'lightbox.save') {
            alert("Lightbox Settings are disabled in free version. If you need those functionalityes, you need to buy the commercial version.");
            Joomla.submitform('save', document.getElementById('adminForm'));
        }
        else if (task == 'lightbox.apply1')
        {    alert("Lightbox Settings are disabled in free version. If you need those functionalityes, you need to buy the commercial version.");
        } else if (task == 'lightbox.cancel') {
            Joomla.submitform('apply1', document.getElementById('adminForm'));
        }
    }
</script>
<div class="slider-options-head">
    <div style="float: left;">
            <div><a href="http://huge-it.com/joomla-extensions-gallery-user-manual/" target="_blank">User Manual</a></div>
            <div>This section allows you to configure the Image Gallery options. <a href="http://huge-it.com/joomla-extensions-gallery-user-manual/" target="_blank">More...</a></div>
            <div>These options are made for pro users, and are displayed only for demonstration. Unfortunatelly free users are unable to use them.</div>
    </div>
    <div style="float: right;">
            <a class="header-logo-text" href="http://huge-it.com/joomla-gallery/" target="_blank">
            <div><img width="177px" src="<?php echo JUri::root() ?>media/com_gallery/images/huge-it1.png" /></div>
                    <div>Get the full version</div>
            </a>
    </div>
</div>
<div style="clear: both"></div>
<div id="j-sidebar-container" class="j-sidebar-container j-sidebar-visible" style="margin: 19px 0 0 -1px;">
    <div id="j-toggle-sidebar-wrapper">
        <div id="j-toggle-sidebar-header" class="j-toggle-sidebar-header" style="padding: 10px 0 0px 20px;">
	<div id="sidebar" class="sidebar">
            <div class="sidebar-nav">
                <ul id="submenu" class="nav nav-list">
                    <li>
                            <a href="index.php?option=com_gallery">Huge - IT Gallery</a>
                    </li>
                    <li>
                            <a href="index.php?option=com_gallery&amp;view=general">General Options</a>
                    </li>
                    <li  class="active">
                            <a href="index.php?option=com_gallery&amp;view=lightbox">Lightbox Options</a>
                    </li>
                    <li>
                            <a href="index.php?option=com_gallery&amp;view=featured">Featured Products</a>
                    </li>
                </ul>
            <div>
	</div>
	<div id="j-toggle-sidebar"></div>
            </div>
		</div>
        </div>
    </div>
</div>
<div id="j-main-container" class="span10 j-toggle-main">
<form action="<?php echo JRoute::_('index.php?option=com_gallery&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate">
    <div class="tab-content">
        <div class="span10" style="width: 100%">
        <div class="back"><fieldset class="form-horizontal">
                <legend class="legend"><?php echo JText::_('Internationalization'); ?></legend>
                        <?php foreach ($this->form->getFieldset('Internationalization') as $field) : ?>
				 <div class="control-group">
                              <?php echo "<div class='control-label'>".$field->label."</div>";
                                    echo "<div class='controls'>".$field->input."</div>";
                                ?>
                                  </div>
			<?php endforeach; ?>
		
            </fieldset></div>
             <div class="back"><fieldset class="form-horizontal">
		<legend class="legend"><?php echo JText::_('Positioning'); ?></legend>
		
			<?php foreach ($this->form->getFieldset('Positioning') as $field) : ?>
				 <div class="control-group">
                              <?php echo "<div class='control-label'>".$field->label."</div>";
                                    echo "<div class='controls'>".$field->input."</div>";
                                ?>
                                </div>
			<?php endforeach; ?>
		
                 </fieldset></div>
       
            <div class="back"><fieldset class="form-horizontal">
		<legend class="legend"><?php echo JText::_('Dimensions'); ?></legend>
		
			<?php foreach ($this->form->getFieldset('Dimensions') as $field) : ?>
				 <div class="control-group">
                              <?php echo "<div class='control-label'>".$field->label."</div>";
                                    echo "<div class='controls'>".$field->input."</div>";
                                ?>
                    </div>
			<?php endforeach; ?>
		
                </fieldset></div>
              <div class="back"><fieldset class="form-horizontal">
		<legend class="legend"><?php echo JText::_('Slideshow'); ?></legend>
		
			<?php foreach ($this->form->getFieldset('Slideshow') as $field) : ?>
				 <div class="control-group">
                              <?php echo "<div class='control-label'>".$field->label."</div>";
                                    echo "<div class='controls'>".$field->input."</div>";
                                ?>
                    </div>
			<?php endforeach; ?>
		
                  </fieldset></div>
        </div>
	<div>
		<input type="hidden" name="task" value="" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
    </div>
</form>
</div>