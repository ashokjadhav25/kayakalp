$(document).ready(function(){
	$('.bxslider').bxSlider({
		auto:true,
		speed:1200,
		mode:'fade'
	});
	$('.bxslider2').bxSlider({
		minSlides: 4,
		maxSlides: 4,
		slideWidth: 210,
		slideMargin: 20,
		auto:true,
		speed:2000,
		moveSlides:1
	});
	$('.bxslider3').bxSlider({
		minSlides: 4,
		maxSlides: 4,
		slideWidth: 275,
		slideMargin: 25,
		auto:false,
		speed:2000,
		moveSlides:1
	});
	
	 $.fn.equalizeHeights = function(){
	   return this.height( Math.max.apply(this, $(this).map(function(i,e){ return $(e).height() }).get() ) )
	 }
	 $('.left-content, .right-content').equalizeHeights();

});



/* Top scroll*/

$(document).ready(function(){
	  jQuery(document).ready(function() {
    var offset = 250;
    var duration = 200;
    jQuery(window).scroll(function() {
     if (jQuery(this).scrollTop() > offset) {
      jQuery('.back-to-top').fadeIn(duration);
     } else {
      jQuery('.back-to-top').fadeOut(duration);
     }
    });

    jQuery('.back-to-top').click(function(event) {
     event.preventDefault();
     jQuery('html, body').animate({scrollTop: 0}, duration);
     return false;
    })
   });
   });